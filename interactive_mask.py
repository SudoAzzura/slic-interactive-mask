import numpy as np
import matplotlib.pyplot as plt
from skimage import io, segmentation
import sys

# Charger une image
image = io.imread(sys.argv[1])
# Créer un plot
fig, ax = plt.subplots()

# Effectuer la segmentation slic
segments = segmentation.slic(image, n_segments=100, compactness=10, sigma=1)


# Définir un compteur pour compter le nombre de superpixels sélectionnés
selected_count = 0
mask = np.zeros_like(image[..., 0],dtype=np.uint8)

# gérer les événements de clic
def onclick(event):
    # derniere condition correspond à lorque l'on save c'est comme si on cliquait sur 0 0
    if event.xdata != None and event.ydata != None and (int(event.xdata) != 0 and int(event.ydata) != 0):
        x, y = int(event.xdata), int(event.ydata)
        superpixel_id = segments[y, x]
        print("Superpixel sélectionné:", superpixel_id,x,y)

        # Mettre à jour le masque
        global mask
        # Si le superpixel est déjà sélectionné, le retirer du masque
        if mask[segments == superpixel_id].all() :
            mask[segments == superpixel_id] = 0
        # Sinon, l'ajouter au masque
        else :
            mask[segments == superpixel_id] = 255
        
        # Mettre à jour l'image affichée
        ax.imshow(segmentation.mark_boundaries(image,mask))
        # ax.contour(mask, colors='red', linewidths=1) # : TRICKY : 08/02/2023 : CHLOE : moins bien 
        
        plt.draw()

def save_mask(event):
    global mask
    result = np.zeros_like(image)
    result[mask == 255] = image[mask == 255]
    io.imsave('masked_image.png', mask)
    print("L'image masquée a été enregistrée sous le nom 'masked_image.png'.")


ax.imshow(image)

# Connecter la fonction de rappel aux événements de clic
cid = fig.canvas.mpl_connect('button_press_event', onclick)

# bouton de save
save_button = plt.axes([0.81, 0.05, 0.1, 0.075])
button = plt.Button(save_button, 'Save', hovercolor='0.975')
button.on_clicked(save_mask)

# Afficher le plot
plt.show()