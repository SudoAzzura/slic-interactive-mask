# Usage

python3 interactive_mask.py [Your image path]

python3 interactive_mask.py [Your image path] [nb of segmentation]

python3 fusionmask.py [Your mask1 path] [Your mask2 path]

# Examples of usage :
 
## Do mask
python3 interactive_mask.py image_test.jpg



To select the mask, click on the areas of the image.
![Alt text](./imageMd/window.JPG?raw=true "Window")

If you click on the "Save" button it gives this mask.
![Alt text](./imageMd/windowmask.png?raw=true "Window")

![Alt text](./imageMd/window2.JPG?raw=true "Window")
![Alt text](./imageMd/window2mask.png?raw=true "Window")

## Fusion 2 mask

python3 fusionmask.py .\maskFusion\masked_image1.png .\maskFusion\masked_image2.png

![Alt text](./maskFusion/masked_image1.png?raw=true "")
![Alt text](./maskFusion/masked_image2.png?raw=true "")



![Alt text](./imageMd/masked_image_fin.png?raw=true "")