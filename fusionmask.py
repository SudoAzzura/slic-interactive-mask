import numpy as np
import matplotlib.pyplot as plt
from skimage import io, segmentation
import sys

# python3 fusionmask.py .\maskaAFusion\masked_image1.png .\maskaAFusion\masked_image2.png
mask1 = io.imread(sys.argv[1])
mask2 = io.imread(sys.argv[2])
print(mask1)
print(mask2)
mask_fusion = np.zeros((*mask1.shape,))


for i in range(mask1.shape[0]):
    for j in range(mask1.shape[1]):
            if(mask1[i][j]==255 or mask2[i][j]==255) :
                mask_fusion[i][j] = 255

io.imsave('masked_image_fin.png', mask_fusion)
print("L'image des masques fusionnés a été enregistrée sous le nom 'masked_image_fin.png'.")